/*==============================================================*/
/* DATABASE :  adms 校友捐赠管理系统                              */
/*==============================================================*/
CREATE DATABASE IF NOT EXISTS `adms`;
USE `adms`;

/*==============================================================*/
/* Table:     constant_type                                     */
/*==============================================================*/
DROP TABLE IF EXISTS `constant_type`;
CREATE TABLE `constant_type`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(100) COMMENT '代码',
  `name` varchar(100) COMMENT '中文名称',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '常数类别';


/*==============================================================*/
/* Table: constant_item                                         */
/*==============================================================*/
DROP TABLE IF EXISTS `constant_item`;
CREATE TABLE `constant_item`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_id` int COMMENT '类别id, 对应constant_type的主键id',
  `code` varchar(100) COMMENT '常数项代码',
  `name` varchar(100) COMMENT '常数项名称',
  `sort` int COMMENT '排序id，选择项显示的时候 从小到大排序',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '常数项';


/*==============================================================*/
/* Table: sys_role                                    */
/*==============================================================*/
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(10) COMMENT '角色名称',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '角色';

/*==============================================================*/
/* Table: sys_permission                                      */
/*==============================================================*/
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` VARCHAR(100) COMMENT '权限名称',
  `url` VARCHAR(100) COMMENT '资源导航路径',
  `icon` VARCHAR(100) COMMENT '菜单显示时的图标',
  `type` int COMMENT '权限类型（需要对应constant_item的id） 目录  导航',
  `parent_id` int DEFAULT 0 COMMENT '上级权限的id，默认根id=0,即无父权限',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '权限';


/*==============================================================*/
/* Table: sys_user                                         */
/*==============================================================*/
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(50) COMMENT '用户名',
  `password` varchar(200) COMMENT '密码',
  `realname` varchar(50) COMMENT '真实姓名',
  `telephone` varchar(15) UNIQUE COMMENT '电话号码',
  `last_login` datetime DEFAULT NULL  COMMENT '最后登录时间',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '用户';


/*==============================================================*/
/* Table: sys_role_permission                                         */
/*==============================================================*/
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int COMMENT '角色id',
  `permissioin_id` varchar(1000) COMMENT '权限id数组,以逗号分割',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '角色权限';

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int COMMENT '用户id',
  `role_id` varchar(1000) COMMENT '角色id数组,以逗号分割',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '用户角色';



/*==============================================================*/
/* Table: donor                                   */
/*==============================================================*/
DROP TABLE IF EXISTS `donor`;
CREATE TABLE `donor`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '捐赠记录id',
  `username` varchar(50) COMMENT '用户名',
  `password` varchar(200) COMMENT '密码',
  `realname` varchar(50) COMMENT '姓名',
  `gender` int COMMENT '性别（需要对应constant_item的id）男，女 未知 ',
  `politic_outlook` int COMMENT '政治面貌（需要对应constant_item的id） ',
  `work_unit` varchar(200) COMMENT '工作单位',
  `job` varchar(100) COMMENT '职务',
  `telephone` varchar(15) COMMENT '电话号码',
  `email` varchar(50) COMMENT '电子邮箱',
  `stock_code` varchar(500) COMMENT '公司股票代码',
  `remarks` varchar(500) COMMENT '备注',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '捐赠记录';


/*==============================================================*/
/* Table: project                                   */
/*==============================================================*/
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目id',
  `name` varchar(100) COMMENT '项目名',
  `leader_name` varchar(50) COMMENT '项目负责人姓名',
  `detail` varchar(500) COMMENT '项目详情',
  `status` int COMMENT '项目状态 （需要对应constant_item的id）',
  `apply_file_url` VARCHAR(500) COMMENT '申请文件地址',
  `approve_file_url` VARCHAR(500) COMMENT '审批文件地址',
  `conclusion_file_url` VARCHAR(500) COMMENT '项目结题文件地址',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '项目';


/*==============================================================*/
/* Table: fund_apply                                   */
/*==============================================================*/
DROP TABLE IF EXISTS `fund_apply`;
CREATE TABLE `fund_apply`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目资金申请id',
  `project_id` int(11) NOT NULL COMMENT '对应项目申请的id',
  `name` varchar(50) COMMENT '资金申请名',
  `cost` decimal(8, 2) COMMENT '申请金额',
  `detail` varchar(500) COMMENT '资金申请详情',
  `status` int COMMENT '资金申请状态 （需要对应constant_item的id）',
  `apply_file_url` VARCHAR(500) COMMENT '申请文件证明地址',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '项目资金申请';

/*==============================================================*/
/* Table: project_donor                                  */
/*==============================================================*/
DROP TABLE IF EXISTS `project_donor`;
CREATE TABLE `project_donor`(
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目捐赠id',
  `project_id` int(11) NOT NULL COMMENT '对应项目申请的id',
  `name` varchar(100) COMMENT '捐赠名目',
  `donor_name` varchar(50) COMMENT '捐赠人姓名',
  `money` decimal(8, 2) COMMENT '捐赠金额',
  `detail` varchar(500) DEFAULT '无' COMMENT '捐赠详情',
  `status` int COMMENT '项目捐赠状态 （需要对应constant_item的id）',
  `treaty_file_url` VARCHAR(500) COMMENT '协议文件地址',
  `donor_approve_url` VARCHAR(500) COMMENT '捐赠方审批文件地址',
  `fund_approve_url` VARCHAR(500) COMMENT '基金会审批文件地址',
  `active` int DEFAULT 1 COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = utf8 COMMENT = '项目捐赠';
