export default {
  // 判断是否为空
  isBlank (val) {
    if (!val) return true
    if (typeof val === 'string') {
      if (val === '') return true
    } else if (val instanceof 'array') {
      if (val.length === 0) return true
    } else if (val instanceof 'object') {
      if (Object.getOwnPropertyNames(val).length === 0) return true
    }
    return false
  }
}
