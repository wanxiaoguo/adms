// 定义一些常量
const TOKEN = 'token'

export default {
  readToken () {
    return window.localStorage.getItem(TOKEN)
  },
  saveToken (val) {
    window.localStorage.setItem(TOKEN, val)
  },
  readArrayBySession (key) {
    return JSON.parse(window.sessionStorage.getItem(key) || '[]')
  },
  writeArrayBySession (key, val) {
    window.sessionStorage.setItem(key, JSON.stringify(val))
  },
  readObjBySession (key) {
    return JSON.parse(window.sessionStorage.getItem(key) || '{}')
  },
  writeObjBySession (key, val) {
    window.sessionStorage.setItem(key, JSON.stringify(val))
  },
  readBySession (key) {
    return window.sessionStorage.getItem(key) || ''
  },
  writeBySession (key, val) {
    window.sessionStorage.setItem(key, val)
  },
  readArrayByLocal (key) {
    return JSON.parse(window.localStorage.getItem(key) || '[]')
  },
  writeArrayByLocal (key, val) {
    window.localStorage.setItem(key, JSON.stringify(val))
  },
  readObjByLocal (key) {
    return JSON.parse(window.localStorage.getItem(key) || '{}')
  },
  writeObjByLocal (key, val) {
    window.localStorage.setItem(key, JSON.stringify(val))
  },
  readByLocal (key) {
    return window.localStorage.getItem(key) || ''
  },
  writeByLocal (key, val) {
    window.localStorage.setItem(key, val)
  }
}
