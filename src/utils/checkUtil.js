export default {
  // 验证手机号的规则
  checkMobile (rule, value, cb) {
    // 验证手机号的正则表达式
    const regMobile = /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/
    if (regMobile.test(value)) {
      return cb()
    }
    cb(new Error('请输入合法的手机号'))
  },
  // 验证邮箱的规则
  checkEmail (rule, value, cb) {
    // 验证邮箱的正则表达式
    const regEmail = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/
    if (regEmail.test(value)) {
      return cb()
    }
    cb(new Error('请输入合法的邮箱'))
  },
  // 大致验证身份证号码的合法性
  checkIdNumber (rule, value, cb) {
    const reg = /^[1-9][0-9]{5}(19|20)[0-9]{2}((01|03|05|07|08|10|12)(0[1-9]|[1-2][0-9]|31)|(04|06|09|11)(0[1-9]|[1-2][0-9]|30)|02(0[1-9]|[1-2][0-9]))[0-9]{3}([0-9]|x|X)$/
    if (reg.test(value)) {
      return cb()
    }
    cb(new Error('请输入合法的身份证号'))
  }
}
