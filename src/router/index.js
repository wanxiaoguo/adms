import Vue from 'vue'
import VueRouter from 'vue-router'
import storeUtil from '@/utils/storageUtils'
import utils from '@/utils/commonUtil'
const Home = () => import('../views/Home.vue')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Login')
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    children: [
      {
        path: '/constant-type',
        name: 'ConstantType',
        component: () => import('@/views/constant-type')
      },
      {
        path: '/constant-item',
        name: 'ConstantItem',
        component: () => import('@/views/constant-item')
      },
      {
        path: '/role',
        name: 'Role',
        component: () => import('@/views/role')
      },
      {
        path: '/user',
        name: 'User',
        component: () => import('@/views/user')
      },
      {
        path: '/permission',
        name: 'Permission',
        component: () => import('@/views/permission')
      },
      {
        path: '/donor',
        name: 'Donor',
        component: () => import('@/views/fund_admin/donor')
      },
      {
        path: '/fund_project',
        name: 'FundProject',
        component: () => import('@/views/fund_admin/fund_project')
      },
      {
        path: '/college_project',
        name: 'CollegeProject',
        component: () => import('@/views/college_charge/college_project')
      },
      {
        path: '/college_project/add',
        name: 'CollegeProjectAdd',
        component: () => import('@/views/college_charge/college_project_add')
      },
      {
        path: '/college_project/detail',
        name: 'CollegeProjectDeail',
        component: () => import('@/views/college_charge/college_project_detail')
      },
      {
        path: '/project',
        name: 'project',
        component: () => import('@/views/fund_project')
      },
      {
        path: '/project_check',
        name: 'project_check',
        component: () => import('@/views/fund_project/check')
      },
      {
        path: '/project_detail',
        name: 'project_detail',
        component: () => import('@/views/fund_project/detail')
      },
      {
        path: '/project_apply',
        name: 'project_apply',
        component: () => import('@/views/fund_project/apply')
      },
      {
        path: '/project_applylist',
        name: 'project_applylist',
        component: () => import('@/views/fund_project/applylist')
      },
      {
        path: '/project_over',
        name: 'project_over',
        component: () => import('@/views/fund_project/over')
      },
      {
        path: '/lovewall',
        name: 'lovewall',
        component: () => import('@/views/fund_project/lovewall')
      }
    ]
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})

// 挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to 将要访问的路径
  // from 代表从哪个路径跳转而来
  // next 是一个函数，表示放行
  //     next()  放行    next('/login')  强制跳转
  if (to.path === '/login') return next()
  // 获取token 判断是否存在
  if (utils.isBlank(storeUtil.readToken())) return next('/login')
  next()
})

export default router
