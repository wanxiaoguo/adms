/*
包含n个用于间接更新状态的方法的对象模块
 */
import { TOGGLE_COLLAPSE, GET_ALL_CONSTANT_TYPE, GET_ROLES } from './mutation-types'
import axios from 'axios'

export default {
  toggleCollapse ({ commit }) {
    commit(TOGGLE_COLLAPSE)
  },
  async getConstantTypes ({ commit }) {
    const { data: res } = await axios.get('/constantTypes/all')
    if (res.code === 200) {
      commit(GET_ALL_CONSTANT_TYPE, res.obj)
    }
  },
  async getRoles ({ commit }) {
    const { data: res } = await axios.get('/roles/all')
    if (res.code === 200) {
      commit(GET_ROLES, res.obj)
    }
  }
}
