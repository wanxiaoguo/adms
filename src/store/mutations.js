/*
包含n个用于直接更新状态的方法的对象模块
 */
import { GET_ROLES, GET_ALL_CONSTANT_TYPE, TOGGLE_COLLAPSE } from './mutation-types'

export default {
  [TOGGLE_COLLAPSE] (state) {
    state.isCollapse = !state.isCollapse
  },
  [GET_ROLES] (state, roles) {
    state.roles = roles
  },
  [GET_ALL_CONSTANT_TYPE] (state, constantTypes) {
    state.constantTypes = constantTypes
  }
}
