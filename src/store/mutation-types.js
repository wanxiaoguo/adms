/*
包含n个mutation名称常量
 */
export const TOGGLE_COLLAPSE = 'toggleCollapse' // 折叠左侧菜单栏
export const GET_ROLES = 'getRoles' // 得到所有的角色
export const GET_ALL_CONSTANT_TYPE = 'getConstanTypes' // 得到所有的常数类别
