module.exports = {
  // 修改或新增html-webpack-plugin的值，在index.html里面能读取htmlWebpackPlugin.options.title
  chainWebpack: config => {
    config.plugin('html')
      .tap(args => {
        args[0].title = '校友捐赠管理系统'
        return args
      })
  },
  /* webpack-dev-server 相关配置 */
  devServer: {
    /* 自动打开浏览器 */
    open: true,
    /* 设置为0.0.0.0则所有的地址均能访问 */
    host: 'localhost',
    // 设置端口号
    port: 80,
    https: false,
    hotOnly: false,
    /* 使用代理 */
    proxy: {
      '/api': {
        ws: false, // proxy websockets
        /* 目标代理服务器地址 */
        target: 'http://localhost:8081',
        /* 允许跨域 */
        changeOrigin: true,
        pathRewrite: {
          '^/api': ''
        } // 真正访问的时候去掉/api
      }
    }
  }
}
