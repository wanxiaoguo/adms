### web架构演进
![wLV2hq.jpg](https://s1.ax1x.com/2020/09/22/wLV2hq.jpg)
![wLVW90.jpg](https://s1.ax1x.com/2020/09/22/wLVW90.jpg)
![wLVgNn.jpg](https://s1.ax1x.com/2020/09/22/wLVgNn.jpg)
![wLVhcT.jpg](https://s1.ax1x.com/2020/09/22/wLVhcT.jpg)
![wLV4jU.jpg](https://s1.ax1x.com/2020/09/22/wLV4jU.jpg)
![wLVIuF.jpg](https://s1.ax1x.com/2020/09/22/wLVIuF.jpg)
![wLVTHJ.jpg](https://s1.ax1x.com/2020/09/22/wLVTHJ.jpg)
![wLVHE9.jpg](https://s1.ax1x.com/2020/09/22/wLVHE9.jpg)
![wLVbNR.jpg](https://s1.ax1x.com/2020/09/22/wLVbNR.jpg)
![wLVq41.jpg](https://s1.ax1x.com/2020/09/22/wLVq41.jpg)
![wLVO9x.jpg](https://s1.ax1x.com/2020/09/22/wLVO9x.jpg)

### linux操作
![wLVoB4.jpg](https://s1.ax1x.com/2020/09/22/wLVoB4.jpg)
![wLARVs.jpg](https://s1.ax1x.com/2020/09/22/wLARVs.jpg)
![wLAsxS.jpg](https://s1.ax1x.com/2020/09/22/wLAsxS.jpg)
![wLA6Kg.jpg](https://s1.ax1x.com/2020/09/22/wLA6Kg.jpg)
![wLAcrQ.jpg](https://s1.ax1x.com/2020/09/22/wLAcrQ.jpg)
![wLAr28.jpg](https://s1.ax1x.com/2020/09/22/wLAr28.jpg)
![wLAgbj.jpg](https://s1.ax1x.com/2020/09/22/wLAgbj.jpg)
![wLAWan.jpg](https://s1.ax1x.com/2020/09/22/wLAWan.jpg)
![wLAf5q.jpg](https://s1.ax1x.com/2020/09/22/wLAf5q.jpg)
![wLA5GV.jpg](https://s1.ax1x.com/2020/09/22/wLA5GV.jpg)
![wLA4P0.jpg](https://s1.ax1x.com/2020/09/22/wLA4P0.jpg)
![wLAI2T.jpg](https://s1.ax1x.com/2020/09/22/wLAI2T.jpg)
![wLA7MF.jpg](https://s1.ax1x.com/2020/09/22/wLA7MF.jpg)
![wLAoxU.jpg](https://s1.ax1x.com/2020/09/22/wLAoxU.jpg)
![wLAHr4.jpg](https://s1.ax1x.com/2020/09/22/wLAHr4.jpg)
![wLAbqJ.jpg](https://s1.ax1x.com/2020/09/22/wLAbqJ.jpg)
![wLAOaR.jpg](https://s1.ax1x.com/2020/09/22/wLAOaR.jpg)
![wLAvPx.jpg](https://s1.ax1x.com/2020/09/22/wLAvPx.jpg)
![wLAXI1.jpg](https://s1.ax1x.com/2020/09/22/wLAXI1.jpg)
![wLAxG6.jpg](https://s1.ax1x.com/2020/09/22/wLAxG6.jpg)

### docker 安装mysql
+ docker 安装 mysql8 `docker pull mysql:latest`

+ 查看镜像 `docker images`

![](img/查看镜像.jpg)

+ 启动mysql `docker run -it --name mysqlTest -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 0d`
  或者运行镜像，设置初始密码、本机与docker端口的映射与挂载本地数据盘 (启动msyql服务)
  `docker run -itd -p 3307:3306 --name test_mysql -v /usr/local/mysqlData/test/conf:/etc/mysql -v /usr/local/mysqlData/test/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 mysql`

![](img/查看正在运行容器.jpg)

+ 以交互式的方式进入mysql `docker exec -it e4 /bin/bash`

![](img/进入mysql.jpg)

```sql
-- 设置权限（为root分配权限，以便可以远程连接）
grant all PRIVILEGES on *.* to root@'%' WITH GRANT OPTION;

ALTER user 'root'@'%' IDENTIFIED BY '123456' PASSWORD EXPIRE NEVER;

-- 更新密码算法，便于使用Navicat连接
ALTER user 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456';

-- 刷新
FLUSH PRIVILEGES;
```

#### docker 导出导入数据库
+ 通过查看`docker ps` 查看mysql启动的容器名
+ 导出数据库 `docker exec -it  mysql_server mysqldump -uroot -p1234 adms > /tmp/adms.sql`
  + `mysql_server` 是mysql启动的容器名
  + `adms` 数据库名
  + `/tmp/adms.sql` 导出到宿主机的目录

+ 进入mysql容器后，登录，执行sql文件 `/tmp/adms.sql`