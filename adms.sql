/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS `adms`;
USE `adms`;


# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: constant_item
# ------------------------------------------------------------

DROP TABLE IF EXISTS `constant_item`;
CREATE TABLE `constant_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type_id` int(11) DEFAULT NULL COMMENT '类别id, 对应constant_type的主键id',
  `code` varchar(100) DEFAULT NULL COMMENT '常数项代码',
  `name` varchar(100) DEFAULT NULL COMMENT '常数项名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序id，选择项显示的时候 从小到大排序',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='常数项';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: constant_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `constant_type`;
CREATE TABLE `constant_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `code` varchar(100) DEFAULT NULL COMMENT '代码',
  `name` varchar(100) DEFAULT NULL COMMENT '中文名称',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='常数类别';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: donor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `donor`;
CREATE TABLE `donor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '捐赠记录id',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `realname` varchar(50) DEFAULT NULL COMMENT '姓名',
  `gender` int(11) DEFAULT NULL COMMENT '性别（需要对应constant_item的id）男，女 未知 ',
  `politic_outlook` int(11) DEFAULT NULL COMMENT '政治面貌（需要对应constant_item的id） ',
  `work_unit` varchar(200) DEFAULT NULL COMMENT '工作单位',
  `job` varchar(100) DEFAULT NULL COMMENT '职务',
  `telephone` varchar(15) DEFAULT NULL COMMENT '电话号码',
  `email` varchar(50) DEFAULT NULL COMMENT '电子邮箱',
  `stock_code` varchar(500) DEFAULT NULL COMMENT '公司股票代码',
  `remarks` varchar(500) DEFAULT NULL COMMENT '备注',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='捐赠记录';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: fund_apply
# ------------------------------------------------------------

DROP TABLE IF EXISTS `fund_apply`;
CREATE TABLE `fund_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目资金申请id',
  `project_id` int(11) NOT NULL COMMENT '对应项目申请的id',
  `name` varchar(50) DEFAULT NULL COMMENT '资金申请名',
  `cost` decimal(8,2) DEFAULT NULL COMMENT '申请金额',
  `detail` varchar(500) DEFAULT NULL COMMENT '资金申请详情',
  `status` int(11) DEFAULT NULL COMMENT '资金申请状态 （需要对应constant_item的id）',
  `apply_file_url` varchar(500) DEFAULT NULL COMMENT '申请文件证明地址',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目资金申请';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目id',
  `name` varchar(100) DEFAULT NULL COMMENT '项目名',
  `leader_name` varchar(50) DEFAULT NULL COMMENT '项目负责人姓名',
  `detail` varchar(500) DEFAULT NULL COMMENT '项目详情',
  `status` int(11) DEFAULT NULL COMMENT '项目状态 （需要对应constant_item的id）',
  `apply_file_url` varchar(500) DEFAULT NULL COMMENT '申请文件地址',
  `approve_file_url` varchar(500) DEFAULT NULL COMMENT '审批文件地址',
  `conclusion_file_url` varchar(500) DEFAULT NULL COMMENT '项目结题文件地址',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: project_donor
# ------------------------------------------------------------

DROP TABLE IF EXISTS `project_donor`;
CREATE TABLE `project_donor` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '项目捐赠id',
  `project_id` int(11) NOT NULL COMMENT '对应项目申请的id',
  `name` varchar(100) DEFAULT NULL COMMENT '捐赠名目',
  `donor_name` varchar(50) DEFAULT NULL COMMENT '捐赠人姓名',
  `money` decimal(8,2) DEFAULT NULL COMMENT '捐赠金额',
  `detail` varchar(500) DEFAULT '无' COMMENT '捐赠详情',
  `status` int(11) DEFAULT NULL COMMENT '项目捐赠状态 （需要对应constant_item的id）',
  `treaty_file_url` varchar(500) DEFAULT NULL COMMENT '协议文件地址',
  `donor_approve_url` varchar(500) DEFAULT NULL COMMENT '捐赠方审批文件地址',
  `fund_approve_url` varchar(500) DEFAULT NULL COMMENT '基金会审批文件地址',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目捐赠';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: sys_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '权限名称',
  `url` varchar(200) DEFAULT NULL COMMENT '资源导航路径',
  `icon` varchar(100) DEFAULT NULL COMMENT '菜单显示时的图标',
  `type` int(11) DEFAULT NULL COMMENT '权限类型（需要对应constant_item的id） 目录  导航',
  `parent_id` int(11) DEFAULT '0' COMMENT '上级权限的id，默认根id=0,即无父权限',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='权限';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: sys_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` char(10) DEFAULT NULL COMMENT '角色名称',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='角色';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: sys_role_permission
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
  `permissioin_id` varchar(1000) DEFAULT NULL COMMENT '权限id数组,以逗号分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='角色权限';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: sys_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `password` varchar(200) DEFAULT NULL COMMENT '密码',
  `realname` varchar(50) DEFAULT NULL COMMENT '真实姓名',
  `telephone` varchar(15) DEFAULT NULL COMMENT '电话号码',
  `last_login` datetime DEFAULT NULL COMMENT '最后登录时间',
  `active` int(11) DEFAULT '1' COMMENT '是否有效： 1 有效 默认 ; 0 无效',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `telephone` (`telephone`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='用户';

# ------------------------------------------------------------
# SCHEMA DUMP FOR TABLE: sys_user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(1000) DEFAULT NULL COMMENT '角色id数组,以逗号分割',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户角色';

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: constant_item
# ------------------------------------------------------------

INSERT INTO `constant_item` (`id`,`type_id`,`code`,`name`,`sort`,`active`,`create_time`,`update_time`) VALUES (1,1,'GENDER_MALE','男',1,1,'2020-09-19 09:25:07','2020-09-19 09:25:07'),(2,1,'GENDER_FEMALE','女',2,1,'2020-09-19 09:25:29','2020-09-19 09:25:29'),(3,1,'GENDER_UNKNOWN','未知',3,1,'2020-09-19 09:26:05','2020-09-19 09:26:05'),(4,2,'party_member','中共党员',1,1,'2020-09-21 16:00:21','2020-09-21 16:00:21'),(5,2,'probationary_party_member','中共预备党员',2,1,'2020-09-21 16:01:11','2020-09-21 16:01:11'),(6,2,'League_member','共青团员',3,1,'2020-09-21 16:01:55','2020-09-21 16:01:55'),(7,2,'masses','群众',4,1,'2020-09-21 16:02:49','2020-09-21 16:02:49'),(8,6,'catalog','目录',1,1,'2020-09-21 16:03:14','2020-09-21 16:03:14'),(9,6,'navigation','导航',2,1,'2020-09-21 16:03:33','2020-09-21 16:03:33'),(10,3,'aduit','待审核',1,1,'2020-09-23 21:30:14','2020-09-23 21:30:14'),(11,3,'in_existence','存续中',2,1,'2020-09-23 21:31:31','2020-09-23 21:31:31'),(12,3,'closed_items','已结项',3,1,'2020-09-23 21:32:06','2020-09-23 21:32:06'),(13,4,'pending_approval','待审批',1,1,'2020-09-23 21:32:39','2020-09-23 21:32:39'),(14,4,'approved','审批通过',2,1,'2020-09-23 21:33:23','2020-09-23 21:33:23'),(15,5,'approved_by_donors','待捐赠方审核',1,1,'2020-09-23 21:34:29','2020-09-23 21:34:29'),(16,5,'approved_by_foundation','待基金会审批',2,1,'2020-09-23 21:35:04','2020-09-23 21:35:04'),(17,5,'successful_donation','捐赠成功',3,1,'2020-09-23 21:35:41','2020-09-23 21:35:41');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: constant_type
# ------------------------------------------------------------

INSERT INTO `constant_type` (`id`,`code`,`name`,`active`,`create_time`,`update_time`) VALUES (1,'GENDER','性别',1,'2020-09-19 09:11:52','2020-09-19 09:11:52'),(2,'POLITIC_OUTLOOK','政治面貌',1,'2020-09-21 15:51:58','2020-09-21 15:51:58'),(3,'RROJECT_STATUS','项目状态',1,'2020-09-21 15:52:39','2020-09-21 15:52:39'),(4,'FUND_APPLY_STATUS','资金申请状态',1,'2020-09-21 15:53:16','2020-09-21 15:53:16'),(5,'PROJECT_DONATE_STATUS','项目捐赠状态',1,'2020-09-21 15:54:56','2020-09-21 15:54:56'),(6,'POWER_TYPE','权限类别',1,'2020-09-21 15:55:42','2020-09-21 15:55:42');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: donor
# ------------------------------------------------------------

INSERT INTO `donor` (`id`,`username`,`password`,`realname`,`gender`,`politic_outlook`,`work_unit`,`job`,`telephone`,`email`,`stock_code`,`remarks`,`active`,`create_time`,`update_time`) VALUES (1,'ant','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','张三',1,7,'京南','Java工程师','15733528352','2464210826@qq.com','11110000','无',1,'2020-09-21 17:47:46','2020-09-21 17:47:46');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: fund_apply
# ------------------------------------------------------------
INSERT INTO `fund_apply` VALUES (1,8,'买饭',298.00,'买资料',2,'http://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916119623&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=e22Ct%2F92b66dKs1RLADDPCPITjw%3D',1,'2020-09-21 15:06:56','2020-09-22 17:07:15'),(2,8,'买资料',99.00,'2',2,'http://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916119623&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=e22Ct%2F92b66dKs1RLADDPCPITjw%3D',1,'2020-09-22 08:45:55','2020-09-22 08:45:55'),(3,8,'买服务器',22.00,'22',2,'',1,'2020-09-22 08:46:06','2020-09-22 19:21:27'),(4,12,'1',0.00,'1',NULL,'group1/M00/00/00/J2RCRF9qtgmACUqRAARBmWsPEk4404.jpg',1,'2020-09-23 02:42:17','2020-09-23 02:42:17');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: project
# ------------------------------------------------------------
INSERT INTO `project` VALUES (8,'智能机器人','刘勇','跟人一样',2,'\nhttp://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916107131&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=1YRcQDxinAhcTNqFMkUf5fVXrKo%3D','http://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%97%B6%E9%97%B4%E5%AE%89%E6%8E%92.png?Expires=1916133706&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=ofJoFRG64j%2FaaS%2FaU1DM%2BiNhczw%3D',NULL,1,'2020-09-22 11:17:48','2020-09-23 11:14:53'),(9,'飞天手表','刘勇','看时间',2,'\nhttp://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916107131&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=1YRcQDxinAhcTNqFMkUf5fVXrKo%3D','http://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916203628&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=fyGIhq8tI6WkOJWSGa%2BOY4vElfw%3D',NULL,1,'2020-09-22 11:18:24','2020-09-23 14:47:14'),(10,'掌上阅读','刘勇','看书',3,'\nhttp://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916107131&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=1YRcQDxinAhcTNqFMkUf5fVXrKo%3D','http://bucketmy.oss-cn-beijing.aliyuncs.com/%E6%89%93%E5%8D%A1%E6%97%B6%E9%97%B4.png?Expires=1916134623&OSSAccessKeyId=LTAI4GCH6t7C928r2NutJpuF&Signature=kfmYmIXN179z1u5MJBizsvcsyv8%3D',NULL,1,'2020-09-22 11:35:02','2020-09-23 14:53:38'),(11,'1','1','1',3,'group1/M00/00/00/J2RCRF9qIU6AGX3SAAXKjQCZo_U471.jpg',NULL,NULL,1,'2020-09-23 00:07:44','2020-09-23 00:07:44'),(12,'花木兰','董京钊','花木兰',3,'group1/M00/00/00/J2RCRF9qn5uALtQSAARBmWsPEk4879.jpg',NULL,NULL,1,'2020-09-23 09:06:38','2020-09-23 09:06:38'),(13,'2','2','2',3,'group1/M00/00/00/J2RCRF9qq_KAQbLBAARBmWsPEk4768.jpg',NULL,NULL,1,'2020-09-23 09:59:17','2020-09-23 09:59:17');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: project_donor
# ------------------------------------------------------------
INSERT INTO `project_donor` VALUES (1,1,NULL,NULL,NULL,'无',NULL,NULL,NULL,NULL,1,'2020-09-21 15:27:16','2020-09-21 15:27:16');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: sys_permission
# ------------------------------------------------------------

INSERT INTO `sys_permission` (`id`,`name`,`url`,`icon`,`type`,`parent_id`,`active`,`create_time`,`update_time`) VALUES (1,'系统管理','','el-icon-user',0,0,1,'2020-09-18 16:51:02','2020-09-18 16:51:02'),(2,'用户列表','user','el-icon-user-solid',1,1,1,'2020-09-18 17:05:18','2020-09-18 17:05:18'),(3,'角色管理','role','el-icon-collection',1,1,1,'2020-09-18 17:08:17','2020-09-18 17:08:17'),(4,'权限管理','permission','el-icon-s-finance',1,1,1,'2020-09-18 17:09:41','2020-09-18 17:09:41'),(5,'基本信息管理','','el-icon-setting',0,0,1,'2020-09-18 17:10:21','2020-09-18 17:10:21'),(6,'常数类别管理','constant-type','el-icon-s-operation',1,5,1,'2020-09-18 17:11:08','2020-09-18 17:11:08'),(7,'常数项目管理','constant-item','el-icon-s-management',1,5,1,'2020-09-18 17:11:56','2020-09-18 17:11:56'),(8,'基金会功能','','el-icon-money',0,0,1,'2020-09-19 09:37:39','2020-09-19 09:37:39'),(9,'项目列表','project','el-icon-tickets',1,8,1,'2020-09-19 09:39:07','2020-09-19 09:39:07'),(10,'捐赠人管理','donor','el-icon-s-custom',1,8,1,'2020-09-19 09:46:49','2020-09-19 09:46:49'),(11,'学院功能','','el-icon-school',0,0,1,'2020-09-19 09:48:16','2020-09-19 09:48:16'),(12,'申请项目列表','college_project','el-icon-tickets',1,11,1,'2020-09-19 09:49:46','2020-09-19 09:49:46'),(13,'项目申请','college_project/add','el-icon-document-add',1,11,1,'2020-09-19 09:51:33','2020-09-19 09:51:33');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: sys_role
# ------------------------------------------------------------

INSERT INTO `sys_role` (`id`,`name`,`active`,`create_time`,`update_time`) VALUES (1,'超级管理员',1,'2020-09-18 17:24:49','2020-09-18 17:24:49'),(2,'管理员',1,'2020-09-18 17:25:03','2020-09-18 17:25:03'),(3,'基金管理员',1,'2020-09-18 17:25:45','2020-09-18 17:25:45'),(4,'老师',1,'2020-09-18 17:25:54','2020-09-18 17:25:54'),(5,'游客',1,'2020-09-18 17:26:11','2020-09-18 17:26:11');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: sys_role_permission
# ------------------------------------------------------------

INSERT INTO `sys_role_permission` (`id`,`role_id`,`permissioin_id`) VALUES (1,1,'1,2,3,4,5,6,7,8,9,10,11,12,13'),(2,2,'5,6,7'),(3,3,'8,9,10'),(4,4,'11,12,13');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: sys_user
# ------------------------------------------------------------

INSERT INTO `sys_user` (`id`,`username`,`password`,`realname`,`telephone`,`last_login`,`active`,`create_time`,`update_time`) VALUES (1,'admin','8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918','超级管理员','18905621669','2020-09-21 17:09:47',1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(2,'admin001','1267057f1ec769c39b88211c4c666382a7dc9fe22f765a66a37918a6a6ab581a','管理员1号','18905631669',NULL,0,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(3,'admin002','1267057f1ec769c39b88211c4c666382a7dc9fe22f765a66a37918a6a6ab581a','管理员2号','18905691669',NULL,0,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(4,'华佗','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','华佗','15305560455',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(5,'孙思邈','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','孙思邈','15305560453',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(6,'李时珍','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','李时珍','15305560451',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(7,'张仲景','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','张仲景','15305560459',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(8,'扁鹊','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','扁鹊','15305560411',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(9,'皇甫谧','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','皇甫谧','15304461455',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49'),(10,'冯兴国','8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92','冯兴国','15305561415',NULL,1,'2020-09-18 11:05:49','2020-09-18 11:05:49');

# ------------------------------------------------------------
# DATA DUMP FOR TABLE: sys_user_role
# ------------------------------------------------------------

INSERT INTO `sys_user_role` (`id`,`user_id`,`role_id`) VALUES (1,1,'1,4,3');

/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
